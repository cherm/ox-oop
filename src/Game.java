import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;
	static char[][] Table = { { '1', '2', '3' }, { '4', '5', '6' }, { '7', '8', '9' } };
	private int row;
	private int column;

	Game() {
		o = new Player('O');
		x = new Player('X');
	}

	public void play() {	
		playAgain();
}


	
private void showStat() {
	System.out.printf("Show \t win\tdraw\tlose\n");
	System.out.printf("%c \t %d \t %d\t %d \n",x.getName(),x.getWin(),x.getDraw(),x.getLose());
	System.out.printf("%c \t %d \t %d\t %d \n\n",o.getName(),o.getWin(),o.getDraw(),o.getLose());
}





private void showTurn() {
	System.out.print("Player "+ board.getPlayer() +", please enter the number [your] "+ board.getCurrent().getName()+" :");
}
public void getPlayer() {
	board.swichTurn();
}

private void showTable() {
	
	System.out.println("");
	System.out.printf("\t\t %c | %c | %c\n",Table[0][0],Table[0][1],Table[0][2]);
	System.out.print("\t\t---+---+---\n");
	System.out.printf("\t\t %c | %c | %c\n",Table[1][0],Table[1][1],Table[1][2]);
	System.out.print("\t\t---+---+---\n");
	System.out.printf("\t\t %c | %c | %c\n",Table[2][0],Table[2][1],Table[2][2]);	
}


private void showWelcome() {
	System.out.printf("\t    Welcome to Game XO.\n");
}

private void input() {
	Scanner kb = new Scanner (System.in);
	int RC = kb.nextInt();
	row =board.getRow(RC);
	column =board.getColumn(RC);
}

private void setTable() {
	if(Table[row][column] =='1'  || Table[row][column] =='2'||Table[row][column] =='3'
			|| Table[row][column] =='4'|| Table[row][column] =='5'|| Table[row][column] =='6'
			|| Table[row][column] =='7'|| Table[row][column] =='8'|| Table[row][column] =='9') {
		Table[row][column] = board.getCurrent().getName();
	}else {
		System.out.println("!!! ERROR !!! ,Please enter the number");
		showTurn();
		input();
	}
}

public void playAgain() {
	board = new Board(x,o);
	showWelcome();
	showTable();
	while(true) {
		showTurn();
		input();
		setTable();
		if(board.isEnd()) {
			showTable();
			showResult();
			break;
		}
		getPlayer();
		showTable();
	}
	showStat();
	
	
}

public void showResult() {
	if(board.getResult()==1) {
		System.out.println("Congratuiations!!, Player " +board.getPlayer()+" ["  +board.getWinner().getName()+  "] "+", YOU ARE THE WINNER!");
		System.out.println("\n");
	}else {
		System.out.printf("\tHow boring, it is a draw\n\n");
	}
	
}
}
