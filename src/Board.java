
public class Board {
	private Game  board;
	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int player;
	private int turnCount;
	private int row;
	private int column;
	private int result;
	
	Board(Player x, Player o){
		this.x = x;
		this.o = o;
		current = x ;
		player = 1;
		turnCount = 0;
		winner = null;
	}
	
	public int getCount() {
		turnCount+=1;
		return turnCount;
	}
	public int getResult() {
		return result;
	}
	
	public boolean checkWin() {
		if( (board.Table[0][0] == board.Table [1][1] && board.Table[0][0] == board.Table [2][2]) ||
				board.Table[0][2] == board.Table [1][1] && board.Table[0][2] == board.Table [2][0]) {
				winner = current;
				if(current == x) {
					x.win();
					o.lose();
				}else {
					o.win();
					x.lose();
				}
				return true;
			}else {
				for(int line=0 ; line <=2 ; line++) {
					if((board.Table[line][0] == board.Table[line][1] && board.Table[line][0] == board.Table[line][2])||
							(board.Table[0][line] == board.Table[1][line] && board.Table[0][line] == board.Table[2][line])) {
						winner = current;
						if(current == x) {
							x.win();
							o.lose();
						}else {
							o.win();
							x.lose();
						}
						System.out.println("Congratuiations!!, player" +player+"["+current+"]"+", YOU ARE THE WINNER!");
						return true;
					}
				}
			}
			return false;
	}
	
	public boolean checkDraw() {
		if(getCount() >=9) {
			x.draw();
			o.draw();
			return true;
		}
		return false;	
	}
	
	public boolean isEnd() {
		if(checkWin()) {
			result=1;
			return true;
		}
		if(checkDraw()) {
			result=2;
			return true;
		}
		return false;
		
	}
	
	
	public int getPlayer() {
		return player;
	}
	
	public Player getCurrent() {
		return current;
		
	}
	public void swichTurn() {
		if(player==2) {
			player--;
			current =x;
		}else {
			player++;
			current = o;
		}
	}
	public int getRow(int go) {
		row = --go/3;
		return  row;
	}
	public int getColumn(int go) {
		column = --go%3;
		return  column;
	}
	public Player getWinner() {
		return winner;
	}
	
	
}
